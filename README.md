![login page](https://gitlab.com/09febrian/fe_test/-/raw/master/public/login.png)

![dashboard page](https://gitlab.com/09febrian/fe_test/-/raw/master/public/dashboard.png)

![master page](https://gitlab.com/09febrian/fe_test/-/raw/master/public/master.png)

# About this repo

This project was developed for Jasa Marga Front End test
Tools: Next JS, Tailwind

### username: `admin`
### password `123`

## Available Scripts

In the project directory, you can run:

### `npm install`
### `npm run build`
### `npm start`
