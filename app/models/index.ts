export type UnitKerja = {
  id: number;
  unit: string;
  status: number;
}

export type Ruas = {
  id: number;
  unit: string;
  status: number;
}