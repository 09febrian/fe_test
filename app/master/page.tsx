"use client"
import React, { useState, useEffect } from "react"
import Header from "../components/Header"
import axios from "../configs/axios"
import ModalImage from "../components/ModalImage"
import ModalForm from "../components/ModalForm"
import { BsPencil, BsEye, BsTrash } from "react-icons/bs"
import ModalDelete from "../components/ModalDelete"
import ModalView from "../components/ModalView"
import { columns } from "../components/Column"
import { AiOutlineSearch } from "react-icons/ai"
import Sidebar from "../components/Sidebar"
import { UnitKerja, Ruas } from "../models"
import dynamic from "next/dynamic"

const Table = dynamic(() => import("react-data-table-component"), {
  ssr: false
})

type SelectedImage = {
  ruas_name: string
  photo_url: string
}

type SelectedData = {
  id: string
  unit_id: string
  ruas_name: string
  long: string
  km_awal: string
  km_akhir: string
  photo_url: string
  doc_url: string
  status: string
}

const Master = () => {
  const [ruas, setRuas] = useState<Ruas[]>([])
  const [unitKerja, setUnitKerja] = useState<UnitKerja[]>([])
  const [modalImage, setModalImage] = useState<boolean>(false)
  const [modalForm, setModalForm] = useState<boolean>(false)
  const [modalDelete, setModalDelete] = useState<boolean>(false)
  const [modalView, setModalView] = useState<boolean>(false)
  const [isUpdate, setIsUpdate] = useState<boolean>(false)
  const [keyword, setKeyword] = useState<string>("")
  const [sidebar, setSidebar] = useState<boolean>(false)
  const [selectedImage, setSelectedImage] = useState<SelectedImage>({
    ruas_name: "",
    photo_url: ""
  })
  const [selectedData, setSelectedData] = useState<SelectedData>({
    id: "",
    ruas_name: "",
    unit_id: "",
    long: "",
    km_awal: "",
    km_akhir: "",
    photo_url: "",
    doc_url: "",
    status: ""
  })

  const getRuas = async () => {
    const res = await axios.get(`/ruas?show=active_only`)
    const result = res.data.data
    setRuas(result)
  }

  const getUnitKerja = async () => {
    const res = await axios.get(`${process.env.NEXT_PUBLIC_API}/unit`)
    const result = res.data.data
    setUnitKerja(result)
  }

  useEffect(() => {
    if (!modalForm || !modalDelete || !modalView) {
      getRuas()
      getUnitKerja()
    }
  }, [modalForm, modalDelete, modalView])

  const handleClickImage = ({ ruas_name, photo_url }: SelectedImage) => {
    setSelectedImage({
      ruas_name,
      photo_url
    })
    setModalImage(!modalImage)
  }

  const handleClickDelete = ({
    id,
    unit_id,
    ruas_name,
    long,
    km_awal,
    km_akhir,
    photo_url,
    doc_url,
    status
  }: SelectedData) => {
    setSelectedData({
      id,
      unit_id,
      ruas_name,
      long,
      km_awal,
      km_akhir,
      photo_url,
      doc_url,
      status
    })
    setModalDelete(!modalDelete)
  }

  const handleClickView = ({
    id,
    ruas_name,
    unit_id,
    long,
    km_awal,
    km_akhir,
    photo_url,
    doc_url,
    status
  }: SelectedData) => {
    setSelectedData({
      id,
      unit_id,
      ruas_name,
      long,
      km_awal,
      km_akhir,
      photo_url,
      doc_url,
      status
    })
    setIsUpdate(false)
    setModalView(!modalView)
  }

  const handleClickUpdate = ({
    id,
    unit_id,
    ruas_name,
    long,
    km_awal,
    km_akhir,
    photo_url,
    doc_url,
    status
  }: SelectedData) => {
    setSelectedData({
      id,
      unit_id,
      ruas_name,
      long,
      km_awal,
      km_akhir,
      photo_url,
      doc_url,
      status
    })
    setIsUpdate(true)
    setModalView(!modalView)
  }

  const expandColumn = [
    ...columns.slice(0, 2),
    {
      name: "Foto",
      maxWidth: "50px",
      selector: (row: any) => (
        <div
          className="bg-primary text-white px-3 py-1 rounded cursor-pointer"
          onClick={() => handleClickImage(row)}
        >
          Lihat file
        </div>
      )
    },
    ...columns.slice(2),
    {
      name: "Aksi",
      selector: (row: any) => (
        <div className="flex flex-row space-x-2 items-center">
          <div
            className="p-2 cursor-pointer flex shrink-0 bg-primary rounded text-white"
            onClick={() => handleClickUpdate(row)}
          >
            <BsPencil />
          </div>
          <div
            className="p-2 cursor-pointer flex shrink-0 bg-secondary rounded text-text"
            onClick={() => handleClickView(row)}
          >
            <BsEye />
          </div>
          <div
            className="p-2 cursor-pointer flex shrink-0 bg-red-500 rounded text-white"
            onClick={() => handleClickDelete(row)}
          >
            <BsTrash />
          </div>
        </div>
      )
    }
  ]

  useEffect(() => {
    if (keyword) {
      const lowerCaseKeyword = keyword.toLowerCase()
      const temp = ruas.filter(
        (item: any) =>
          item.ruas_name.toLowerCase().includes(lowerCaseKeyword) ||
          item.km_awal.toLowerCase().includes(lowerCaseKeyword) ||
          item.km_akhir.toLowerCase().includes(lowerCaseKeyword) ||
          item.long == keyword ||
          item.unit_id == keyword
      )
      setRuas(temp)
    } else {
      getRuas()
    }
  }, [keyword])

  return (
    <main className="relative">
      <Header sidebar={sidebar} setSidebar={setSidebar} />
      <Sidebar sidebar={sidebar} setSidebar={setSidebar} />
      <section className="min-h-screen w-full pt-20 md:pt-28">
        <div className="max-w-7xl mx-auto bg-gray-200 h-full w-full p-5 min-h-[80vh]">
          <div className="px-5 py-2 rounded bg-primary text-secondary">
            <div className="flex flex-row justify-between items-center">
              <h1 className="text-xl md:text-2xl font-bold uppercase">
                Master Data
              </h1>
              <div className="flex flex-row items-center space-x-2">
                <div className="relative">
                  <AiOutlineSearch className="absolute right-1 top-1 text-xl" />
                  <input
                    className="input !border-white placeholder:italic placeholder:text-xs text-white"
                    placeholder="type and enter to search"
                    value={keyword}
                    onChange={(e) => setKeyword(e.target.value)}
                  />
                </div>
                <button
                  className="bg-secondary text-text text-sm font-semibold px-3 py-1 rounded"
                  onClick={() => setModalForm(true)}
                >
                  Tambah
                </button>
              </div>
            </div>
          </div>

          <div className="h-full w-full bg-white shadow mt-5 p-5">
            <Table
              columns={expandColumn}
              data={ruas}
              pagination
              paginationPerPage={10}
              paginationRowsPerPageOptions={[5, 10, 20]}
            />
          </div>
        </div>
      </section>

      {modalImage && (
        <ModalImage
          selectedImage={selectedImage}
          setModalImage={setModalImage}
          modalImage={modalImage}
        />
      )}

      {modalForm && (
        <ModalForm
          setModalForm={setModalForm}
          modalForm={modalForm}
          unitKerja={unitKerja}
        />
      )}

      {modalView && (
        <ModalView
          isUpdate={isUpdate}
          setModalView={setModalView}
          modalView={modalView}
          unitKerja={unitKerja}
          selectedData={selectedData}
        />
      )}

      {modalDelete && (
        <ModalDelete
          setModalDelete={setModalDelete}
          modalDelete={modalDelete}
          selectedData={selectedData}
        />
      )}
    </main>
  )
}

export default Master
