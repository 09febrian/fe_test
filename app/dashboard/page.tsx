"use client"
import React, { useState, useEffect } from "react"
import Header from "../components/Header"
import axios from "../configs/axios"
import ColumnChart from "../components/ColumnChart"
import PieChart from "../components/PieChart"
import { columns } from "../components/Column"
import ModalImage from "../components/ModalImage"
import Sidebar from "../components/Sidebar"
import dynamic from "next/dynamic"
import { Ruas } from "../models/index"

const Table = dynamic(() => import("react-data-table-component"), {
  ssr: false
})

type SelectedImage = {
  ruas_name: string
  photo_url: string
}

const Dashboard = () => {
  const [ruas, setRuas] = useState<Ruas[]>([])
  const [seriesColumnChart, setSeriesColumnChart] = useState<number[]>([])
  const [labelColumnChart, setLabelColumnChart] = useState<string[]>([])
  const [modalImage, setModalImage] = useState<boolean>(false)
  const [sidebar, setSidebar] = useState<boolean>(false)
  const [selectedImage, setSelectedImage] = useState({
    ruas_name: "",
    photo_url: ""
  })

  const getRuas = async () => {
    const res = await axios.get(`/ruas?show=active_only`)
    const result = res.data.data
    setRuas(result)
    const groups: any = result.reduce((agg: any, curr: any) => {
      if (agg[`Unit ${curr.unit_id}`]) {
        agg[`Unit ${curr.unit_id}`] += 1
      } else {
        agg[`Unit ${curr.unit_id}`] = 1
      }
      return agg
    }, {})
    setLabelColumnChart(Object.keys(groups))
    setSeriesColumnChart(Object.values(groups))
  }

  useEffect(() => {
    getRuas()
  }, [])

  const handleClickImage = ({ ruas_name, photo_url }: SelectedImage) => {
    setSelectedImage({
      ruas_name,
      photo_url
    })
    setModalImage(!modalImage)
  }

  const expandColumn = [
    ...columns.slice(0, 2),
    {
      name: "Foto",
      maxWidth: "50px",
      selector: (row: any) => (
        <div
          className="bg-primary text-white px-3 py-1 rounded cursor-pointer"
          onClick={() => handleClickImage(row)}
        >
          Lihat file
        </div>
      )
    },
    ...columns.slice(2)
  ]

  return (
    <main className="relative">
      <Header sidebar={sidebar} setSidebar={setSidebar} />
      <Sidebar sidebar={sidebar} setSidebar={setSidebar} />
      <section className="min-h-screen w-full pt-28">
        <div className="max-w-7xl mx-auto bg-gray-200 h-full w-full p-5">
          <div className="px-5 py-2 rounded bg-primary text-secondary">
            <h1 className="text-xl md:text-2xl font-bold uppercase">
              Dashboard
            </h1>
          </div>

          <div className="min-h-[50vh] w-full bg-white shadow mt-5">
            <div className="grid grid-cols-1 lg:grid-cols-2 gap-1">
              <div className="h-full w-full p-5">
                <ColumnChart
                  series={seriesColumnChart}
                  label={labelColumnChart}
                />
              </div>
              <div className="h-full w-full grid place-content-center">
                <PieChart series={seriesColumnChart} label={labelColumnChart} />
              </div>
            </div>
          </div>

          <div className="h-full w-full bg-white shadow mt-5 p-5">
            <Table
              columns={expandColumn}
              data={ruas}
              pagination
              paginationPerPage={10}
              paginationRowsPerPageOptions={[5, 10, 20]}
            />
          </div>
        </div>
      </section>

      {modalImage && (
        <ModalImage
          selectedImage={selectedImage}
          setModalImage={setModalImage}
          modalImage={modalImage}
        />
      )}
    </main>
  )
}

export default Dashboard
