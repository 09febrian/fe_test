import axios from "axios"
export const customAxios = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API
})

customAxios.interceptors.request.use(function (config) {
  const token = localStorage.getItem("token")
  config.headers.Authorization = token ? `Bearer ${token}` : ""
  return config
})

customAxios.interceptors.response.use(
  (response) => response,
  (error) => {
    if (
      error.response.status === 401 &&
      error.response.data.message.toLowerCase() == "api token is required."
    ) {
      localStorage.removeItem("token")
      window.location.href = "/"
    }
    return Promise.reject(error.response.data)
  }
)

export default customAxios
