"use client"
import React, { useRef, useState } from "react"
import axios from "../configs/axios"
import { UnitKerja } from "../models"

interface Props {
  modalForm: boolean
  setModalForm: (modalForm: boolean) => void
  unitKerja: UnitKerja[]
}

const ModalForm = (props: Props) => {
  const { modalForm, setModalForm, unitKerja } = props
  const photoRef = useRef<HTMLInputElement>(null)
  const fileRef = useRef<HTMLInputElement>(null)
  const [photoFileName, setPhotoFileName] = useState("")
  const [fileName, setFileName] = useState("")
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState("")
  const [data, setData] = useState<{ [key: string]: any }>({
    unit_id: "",
    ruas_name: "",
    long: "",
    km_awal: "",
    km_akhir: "",
    status: "0",
    file: File,
    photo: File
  })

  const handleClickPhoto = () => {
    // 👇️ open file input box on click of another element
    if (photoRef.current !== null) {
      photoRef.current.click()
    }
  }

  const handleClickFile = () => {
    // 👇️ open file input box on click of another element
    if (fileRef.current != null) {
      fileRef.current.click()
    }
  }

  const handlePhotoChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const fileObj = event.target.files && event.target.files[0]
    if (!fileObj) {
      return
    }
    setPhotoFileName(fileObj.name)
    setData({
      ...data,
      photo: fileObj
    })
  }

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const fileObj = event.target.files && event.target.files[0]
    if (!fileObj) {
      return
    }
    // event.target.value = null
    setFileName(fileObj.name)
    setData({
      ...data,
      file: fileObj
    })
  }

  const handleSubmit = async () => {
    setLoading(true)
    try {
      const res = await axios.post(`/ruas`, data, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      })
      setModalForm(false)
      setLoading(false)
    } catch (error) {
      setLoading(false)
    }
  }

  return (
    <div
      className="fixed top-0 w-full h-full bg-black/50 z-30 flex items-center justify-center"
      onClick={() => setModalForm(false)}
    >
      <div
        className="min-h-[40vh] p-10 aspect-auto bg-white shadow flex flex-col items-center justify-center rounded"
        onClick={(e) => e.stopPropagation()}
      >
        <h1 className="mb-5 uppercase">Tambah Ruas</h1>

        <div className="grid grid-cols-2 gap-5">
          <div>
            <div className="flex flex-col mb-3">
              <label className="mb-1">Ruas</label>
              <input
                type="text"
                className="bg-gray-100 px-3 py-1 text-xs"
                value={data.ruas_name}
                onChange={(e) =>
                  setData({
                    ...data,
                    ruas_name: e.target.value
                  })
                }
              />
            </div>
            <div className="flex flex-col mb-3">
              <label className="mb-1">Unit kerja</label>
              <select
                className="bg-gray-100 px-3 py-2 text-xs"
                value={data.unit_id}
                onChange={(e) =>
                  setData({
                    ...data,
                    unit_id: e.target.value
                  })
                }
              >
                {unitKerja.map((item: any, key: number) => (
                  <option key={key} value={item.id}>
                    {item.unit}
                  </option>
                ))}
              </select>
            </div>
            <div className="flex flex-col mb-3">
              <label className="mb-1">Foto</label>
              <input
                type="file"
                className="hidden"
                ref={photoRef}
                // onChange={event => console.log(event)}
                onChange={handlePhotoChange}
              />
              <div className="pl-3 pr-1 py-1 bg-gray-100 flex justify-between items-center">
                <span className="text-xs">{photoFileName}</span>
                <div
                  className="text-xs bg-black px-3 py-1 text-white cursor-pointer"
                  onClick={handleClickPhoto}
                >
                  Pilih file
                </div>
              </div>
            </div>

            <div className="flex flex-col mb-3">
              <label className="mb-1">Dokumen</label>
              <input
                type="file"
                className="hidden"
                ref={fileRef}
                onChange={handleFileChange}
              />
              <div className="pl-3 pr-1 py-1 bg-gray-100 flex justify-between items-center">
                <span className="text-xs">{fileName}</span>

                <div
                  className="text-xs bg-black px-3 py-1 text-white cursor-pointer"
                  onClick={handleClickFile}
                >
                  Pilih file
                </div>
              </div>
            </div>
          </div>
          <div>
            <div className="flex flex-col mb-3">
              <label className="mb-1">Panjang (km)</label>
              <input
                type="text"
                className="bg-gray-100 px-3 py-1 text-xs"
                value={data.long}
                onChange={(e) =>
                  setData({
                    ...data,
                    long: e.target.value
                  })
                }
              />
            </div>
            <div className="flex flex-col mb-3">
              <label className="mb-1">Km awal</label>
              <input
                type="text"
                className="bg-gray-100 px-3 py-1 text-xs"
                value={data.km_awal}
                onChange={(e) =>
                  setData({
                    ...data,
                    km_awal: e.target.value
                  })
                }
              />
            </div>
            <div className="flex flex-col mb-3">
              <label className="mb-1">Km akhir</label>
              <input
                type="text"
                className="bg-gray-100 px-3 py-1 text-xs"
                value={data.km_akhir}
                onChange={(e) =>
                  setData({
                    ...data,
                    km_akhir: e.target.value
                  })
                }
              />
            </div>
            <div className="flex flex-col">
              <label className="mb-1">Status</label>
              <select
                className="bg-gray-100 px-3 py-2 text-xs"
                value={data.status}
                onChange={(e) =>
                  setData({
                    ...data,
                    status: e.target.value
                  })
                }
              >
                <option value={1}>Aktif</option>
                <option value={0}>Tidak Aktif</option>
              </select>
            </div>
          </div>
        </div>

        <div className="flex justify-end w-full py-3 mt-10">
          <button
            className="bg-primary text-white text-sm px-3 py-2"
            disabled={loading}
            onClick={handleSubmit}
          >
            {loading ? `Process..` : `Submit`}
          </button>
        </div>
      </div>
    </div>
  )
}

export default ModalForm
