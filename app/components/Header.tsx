"use client"
import React, { useState } from "react"
import Logo from "./Logo"
import { FaRegUserCircle } from "react-icons/fa"
import { HiMenuAlt3 } from "react-icons/hi"
import { usePathname } from "next/navigation"
import Link from "next/link"
import axios from "../configs/axios"
import { useRouter } from "next/navigation"

interface Props {
  setSidebar: (sidebar: boolean) => void
  sidebar: boolean
}

const Header = (props: Props) => {
  const { setSidebar, sidebar } = props
  const pathname = usePathname()
  const [openMenu, setOpenMenu] = useState(false)
  const router = useRouter()

  const handleLogout = async () => {
    const res = await axios.post(`/logout`, {
      username: "admin",
      password: "123"
    })
    localStorage.removeItem("token")
    router.replace("/")
  }

  return (
    <div className="fixed top-0 z-20 w-full flex flex-row justify-between items-center px-5 md:px-10 py-1 bg-white text-text shadow">
      <div className="flex flex-row items-center space-x-10">
        <div className="mr-10">
          <Logo navbarLogo />
        </div>
        <Link href={`/dashboard`} className="hidden md:block">
          <span className={`${pathname.includes("dashboard") && `active`}`}>
            Dashboard
          </span>
        </Link>
        <Link href={`/master`} className="hidden md:block">
          <span className={`${pathname.includes("master") && `active`}`}>
            Master Data
          </span>
        </Link>
      </div>

      <div className="flex flex-row items-center space-x-10 text-3xl text-text relative">
        <FaRegUserCircle
          className="cursor-pointer"
          onClick={() => setOpenMenu(!openMenu)}
        />
        <HiMenuAlt3
          onClick={() => setSidebar(true)}
          className="cursor-pointer"
        />
        {openMenu && (
          <div className="absolute z-30 top-[150%] right-[20%] bg-primary text-white px-8 py-5 shadow text-left">
            <div className="text-sm border-b-[1px] border-white pb-3">
              Account
            </div>
            <div className="text-sm mt-3 cursor-pointer" onClick={handleLogout}>
              Logout
            </div>
          </div>
        )}
      </div>
    </div>
  )
}

export default Header
