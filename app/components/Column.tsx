type DataRow = {
  ruas_name: string
  km_awal: string
  km_akhir: string
  photo_url: string
  doc_url: string
  unit_id: string
}

export const columns: any = [
  {
    name: "Ruas",
    selector: (row: any) => row.ruas_name
  },
  {
    name: "Lokasi",
    selector: (row: any) => `${row.km_awal} s/d ${row.km_akhir}`
  },
  {
    name: "Dokumen",
    maxWidth: "50px",
    selector: (row: any) => (
      <a href={row.doc_url} target="_blank" rel="noopener noreferrer">
        <div className="bg-primary text-white px-3 py-1 rounded">
          Lihat file
        </div>
      </a>
    )
  },
  {
    name: "Unit Kerja",
    selector: (row: any) => `Unit Kerja ${row.unit_id}`
  },
  {
    name: "Status",
    maxWidth: "50px",
    selector: (row: any) =>
      row.status == 1 ? (
        <div className="bg-green-100 text-text px-2 py-1 text-xs">Aktif</div>
      ) : (
        <div className="bg-red-100 text-text px-2 py-1 text-xs">
          Tidak aktif
        </div>
      )
  }
]
