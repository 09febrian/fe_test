import dynamic from "next/dynamic"
import React from "react"

const ReactApexChart = dynamic(() => import("react-apexcharts"), {
  ssr: false
})

type Props = {
  series: number[]
  label: string[]
}

const PieChart = ({ series, label }: Props) => {
  const options: any = {
    chart: {
      width: 380,
      type: "pie"
    },
    labels: label,
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: "bottom"
          }
        }
      }
    ]
  }
  return (
    <ReactApexChart options={options} series={series} type="pie" width={450} />
  )
}

export default PieChart
