import dynamic from "next/dynamic"
import React from "react"
const ReactApexChart = dynamic(() => import("react-apexcharts"), {
  ssr: false
})

type Props = {
  series: number[]
  label: string[]
}

const ColumnChart = ({ series, label }: Props) => {
  const data = [
    {
      data: series
    }
  ]
  const options: any = {
    chart: {
      height: 350,
      type: "bar",
      toolbar: {
        show: false
      }
    },
    plotOptions: {
      bar: {
        columnWidth: "50%",
        distributed: true
      }
    },
    dataLabels: {
      enabled: false
    },
    legend: {
      show: false
    },
    yaxis: {
      title: {
        text: "Jumlah Ruas"
      }
    },
    xaxis: {
      categories: label,
      labels: {
        style: {
          // colors: [
          //   "#013e98",
          //   "#013e98",
          //   "#013e98",
          //   "#013e98",
          //   "#013e98",
          //   "#013e98",
          //   "#013e98",
          //   "#013e98"
          // ],
          fontSize: "12px"
        }
      }
    },
    tooltip: {
      custom: function ({
        series,
        seriesIndex,
        dataPointIndex,
        w
      }: {
        series: any
        seriesIndex: any
        dataPointIndex: any
        w: any
      }) {
        return (
          '<div class="bg-primary text-white px-3 py-1 rounded border-none">' +
          "<span>" +
          w.globals.labels[dataPointIndex] +
          " : " +
          series[seriesIndex][dataPointIndex] +
          " Ruas </span>" +
          "</div>"
        )
      }
    }
  }
  return (
    <ReactApexChart options={options} series={data} type="bar" height={450} />
  )
}

export default ColumnChart
