"use client"
import React from "react"
import Link from "next/link"
import { usePathname } from "next/navigation"
import { AiOutlineCloseCircle } from "react-icons/ai"

interface Props {
  sidebar: boolean
  setSidebar: (sidebar: boolean) => void
}

const Sidebar = (props: Props) => {
  const { sidebar, setSidebar } = props
  const pathname = usePathname()

  return (
    <div
      className={`fixed top-0 w-full h-full z-20 ${
        sidebar ? `translate-x-0 bg-black/50` : `translate-x-full bg-black/0`
      } transition duration-300 ease-in-out backdrop-blur`}
      onClick={() => setSidebar(false)}
    >
      <div className="fixed right-[81%] md:right-[21%] top-10 text-white text-3xl cursor-pointer">
        <AiOutlineCloseCircle onClick={() => setSidebar(false)} />
      </div>
      <div
        className={`fixed right-0 w-[80%] md:w-[20%] h-full bg-white flex flex-col items-center justify-center ${
          sidebar ? `translate-x-0` : `translate-x-full`
        } transition duration-300 ease-in-out delay-200`}
        onClick={(e) => e.stopPropagation()}
      >
        <div className="grid grid-cols-1 gap-5">
          <Link href={`/dashboard`} className="justify-self-center">
            <span className={`${pathname.includes("dashboard") && `active`}`}>
              Dashboard
            </span>
          </Link>
          <Link href={`/master`} className="justify-self-center">
            <span className={`${pathname.includes("master") && `active`}`}>
              Master Data
            </span>
          </Link>
        </div>
      </div>
    </div>
  )
}

export default Sidebar
