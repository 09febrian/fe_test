import React, { useState } from "react"
import axios from "../configs/axios"
const token = `eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vMzQuMTAxLjE0NS40OTo4MDA0L2FwaS9sb2dpbiIsImlhdCI6MTY5MjA4MjI4MCwiZXhwIjoxNjkyMDg1ODgwLCJuYmYiOjE2OTIwODIyODAsImp0aSI6ImRUd1BGN2dGZkJiQWk2anYiLCJzdWIiOiIxIiwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.tiiDJ6pI4D7u9kOSTMyKtpMbUn8P57xoQWrkVxpRaq0`

interface Props {
  modalDelete: boolean
  setModalDelete: (modalDelete: boolean) => void
  selectedData: {
    id: string
    ruas_name: string
  }
}

const ModalDelete = (props: Props) => {
  const { modalDelete, setModalDelete, selectedData } = props
  const [loading, setLoading] = useState(false)

  const handleDelete = async () => {
    setLoading(true)
    try {
      const res = await axios.delete(`/ruas/${selectedData.id}`)
      setModalDelete(false)
      setLoading(false)
    } catch (error) {
      setLoading(false)
    }
  }

  return (
    <div
      className="fixed top-0 w-full h-full bg-black/50 z-30 flex items-center justify-center"
      onClick={() => setModalDelete(false)}
    >
      <div
        className="p-10 aspect-auto bg-white shadow flex flex-col items-center justify-center rounded"
        onClick={(e) => e.stopPropagation()}
      >
        <h1 className="font-semibold mb-5">Konfirmasi Hapus Ruas</h1>
        <p className="text-sm">
          Apakah anda yakin ingin menghapus data {selectedData.ruas_name} ?
        </p>
        <div className="flex flex-row w-full justify-end items-center space-x-2 mt-10">
          <button
            className="bg-red-500 text-white px-3 py-1"
            onClick={() => setModalDelete(false)}
          >
            Tidak
          </button>
          <button
            className="bg-primary text-white px-3 py-1"
            onClick={handleDelete}
          >
            {loading ? `Memproses..` : `Ya`}
          </button>
        </div>
      </div>
    </div>
  )
}

export default ModalDelete
