import Image from "next/image"
import React from "react"

interface Props {
  modalImage: boolean
  setModalImage: (modalImage: boolean) => void
  selectedImage: {
    ruas_name: string
    photo_url: string
  }
}

const ModalImage = (props: Props) => {
  const { modalImage, setModalImage, selectedImage } = props

  return (
    <div
      className="fixed top-0 w-full h-full bg-black/50 z-30 flex items-center justify-center"
      onClick={() => setModalImage(false)}
    >
      <div className="min-h-[60vh] aspect-square bg-white shadow flex flex-col items-center justify-center rounded">
        <h1 className="mb-5 uppercase">{selectedImage.ruas_name}</h1>
        <div className="h-[20rem] aspect-video relative">
          <Image
            src={selectedImage.photo_url}
            alt=""
            fill
            className="object-contain object-center"
          />
        </div>
      </div>
    </div>
  )
}

export default ModalImage
