import Image from "next/image"
import Link from "next/link"
import React from "react"

interface Props {
  navbarLogo: boolean
}

const Logo = ({ navbarLogo }: Props) => {
  return (
    <div
      className={`${
        navbarLogo ? `h-20` : `h-20`
      } aspect-video relative flex justify-center w-full`}
    >
      <Link href={"/"}>
        <Image
          src={"/logo.png"}
          alt="main logo"
          fill
          className="object-contain"
        />
      </Link>
    </div>
  )
}

export default Logo
