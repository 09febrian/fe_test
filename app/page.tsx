"use client"
import Image from "next/image"
import React, { useState, useEffect } from "react"
import Logo from "./components/Logo"
import axios from "./configs/axios"
import { useRouter } from "next/navigation"

const Login = () => {
  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState("")
  const router = useRouter()

  const handleSubmit = async (e: React.SyntheticEvent) => {
    e.preventDefault()
    setLoading(true)
    setError("")
    try {
      const res = await axios.post(`/login`, {
        username,
        password
      })
      localStorage.setItem("token", res.data.access_token)
      setLoading(false)
      setError("")
      router.replace("/dashboard")
    } catch (error: any) {
      setLoading(false)
      setError(JSON.stringify(error?.message))
    }
  }

  useEffect(() => {
    if (typeof window !== "undefined") {
      // Client-side-only code
      const token = localStorage.getItem("token")
      if (token) {
        router.replace("/dashboard")
      }
    }
  }, [])

  return (
    <section className="min-h-screen w-full bg-red-200 grid grid-cols-1 md:grid-cols-2">
      <div className="w-full h-full bg-primary flex flex-col justify-center">
        <div className="max-w-5xl md:min-w-[50%] mx-auto bg-white shadow py-10 px-20">
          <Logo navbarLogo={false} />
          <div className="my-5">
            {error && (
              <div className="bg-red-500 font-semibold text-sm text-white rounded px-3 py-1">
                {error}
              </div>
            )}
          </div>
          <form onSubmit={handleSubmit}>
            <div>
              <div className="flex flex-col">
                <label className="mb-1">Username</label>
                <input
                  type="text"
                  className="input"
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                />
              </div>
            </div>

            <div className="mt-3">
              <div className="flex flex-col">
                <label className="mb-1">Password</label>
                <input
                  type="password"
                  className="input"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </div>
            </div>

            <div className="mt-10 w-full flex justify-end">
              <button
                className="bg-primary text-white px-5 py-3"
                disabled={loading}
                type="submit"
              >
                {loading ? `Memproses..` : `Login`}
              </button>
            </div>
          </form>
        </div>
      </div>

      <div className="w-full h-full relative hidden flex-col md:flex justify-center items-center">
        <Image
          src="/illustration.jpg"
          className="object-cover brightness-50"
          fill
          alt="illustration"
        />

        <div className="z-10 max-w-2xl px-5">
          <h5 className="text-6xl font-bold text-white">VISION</h5>
          <p className="text-white text-2xl font-light mt-5">
            Menjadi Perusahaan Jalan Tol Nasional Terbesar, Terpercaya, dan
            Berkesinambungan.
          </p>
        </div>
      </div>
    </section>
  )
}

export default Login
